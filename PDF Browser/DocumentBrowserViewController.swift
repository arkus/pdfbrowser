//
//  DocumentBrowserViewController.swift
//  PDF Browser
//
//  Created by Mac on 19/10/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import PSPDFKitUI

class DocumentBrowserViewController: UIDocumentBrowserViewController, UIDocumentBrowserViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        allowsDocumentCreation = false
        allowsPickingMultipleItems = false
        
        // Update the style of the UIDocumentBrowserViewController
        browserUserInterfaceStyle = .dark
        view.tintColor = .orange
        
        // Specify the allowed content types of your application via the Info.plist.
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    // MARK: UIDocumentBrowserViewControllerDelegate
    
//    func documentBrowser(_ controller: UIDocumentBrowserViewController, didRequestDocumentCreationWithHandler importHandler: @escaping (URL?, UIDocumentBrowserViewController.ImportMode) -> Void) {
//        let newDocumentURL: URL? = nil
//
//        // Set the URL for the new document here. Optionally, you can present a template chooser before calling the importHandler.
//        // Make sure the importHandler is always called, even if the user cancels the creation request.
//        if newDocumentURL != nil {
//            importHandler(newDocumentURL, .move)
//        } else {
//            importHandler(nil, .none)
//        }
//    }
    
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didPickDocumentURLs documentURLs: [URL]) {
        guard let sourceURL = documentURLs.first else { return }
        
        // Present the Document View Controller for the first document that was picked.
        // If you support picking multiple items, make sure you handle them all.
        presentDocument(at: sourceURL)
    }
    
    func documentBrowser(_ controller: UIDocumentBrowserViewController, didImportDocumentAt sourceURL: URL, toDestinationURL destinationURL: URL) {
        // Present the Document View Controller for the new newly created document
        presentDocument(at: destinationURL)
    }
    
    func documentBrowser(_ controller: UIDocumentBrowserViewController, failedToImportDocumentAt documentURL: URL, error: Error?) {
        // Make sure to handle the failed import appropriately, e.g., by presenting an error message to the user.
    }
    
    // MARK: Document Presentation
    
    func presentDocument(at documentURL: URL) {
        if documentURL.startAccessingSecurityScopedResource() {
            let configuration = PSPDFConfiguration(builder: { builder in
                builder.thumbnailBarMode = .scrubberBar
                builder.isPageLabelEnabled = false
                builder.isTextSelectionEnabled = false
                builder.isAutosaveEnabled = false
                builder.applicationActivities = [
                    UIActivityType.mail,
                    UIActivityType.message,
                ]
                builder.excludedActivityTypes = [
                    PSPDFActivityTypeOpenIn,
                    UIActivityType.print.rawValue,
                    UIActivityType.copyToPasteboard.rawValue,
                    UIActivityType.assignToContact.rawValue,
                    UIActivityType.postToFacebook.rawValue,
                    UIActivityType.postToTwitter.rawValue
                ]
            })
            let doc = PSPDFDocument(url: documentURL)
            let pdfController = PSPDFViewController(document: doc, configuration: configuration)
            var r = pdfController.navigationItem.rightBarButtonItems(for: .document)
            if r != nil {
                if let index = r?.index(of: pdfController.activityButtonItem) {
                    r?.remove(at: index)
                }
                pdfController.navigationItem.setRightBarButtonItems(r!, for: .document, animated: true)
            }
            self.present(PSPDFNavigationController(rootViewController: pdfController), animated: true, completion: nil)
        }
    }
}

