//
//  DocumentViewController.swift
//  PDF Browser
//
//  Created by Mac on 19/10/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import PSPDFKitUI

class DocumentViewController: UIViewController {
    
    @IBOutlet weak var documentNameLabel: UILabel!
    
    var fileUrl: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Access the document
        guard let fileUrl = self.fileUrl else { return }
        if fileUrl.startAccessingSecurityScopedResource() {
            let configuration = PSPDFConfiguration(builder: { builder in
                builder.thumbnailBarMode = .scrubberBar
                builder.isPageLabelEnabled = false
                builder.isTextSelectionEnabled = false
                builder.isAutosaveEnabled = false
                builder.applicationActivities = [
                    UIActivityType.mail,
                    UIActivityType.message,
                ]
                builder.excludedActivityTypes = [
                    PSPDFActivityTypeOpenIn,
                    UIActivityType.print.rawValue,
                    UIActivityType.copyToPasteboard.rawValue,
                    UIActivityType.assignToContact.rawValue,
                    UIActivityType.postToFacebook.rawValue,
                    UIActivityType.postToTwitter.rawValue
                ]
            })
            let doc = PSPDFDocument(url: fileUrl)
            let pdfController = PSPDFViewController(document: doc, configuration: configuration)
            var r = pdfController.navigationItem.rightBarButtonItems(for: .document)
            if r != nil {
                if let index = r?.index(of: pdfController.activityButtonItem) {
                    r?.remove(at: index)
                }
                pdfController.navigationItem.setRightBarButtonItems(r!, for: .document, animated: true)
            }
            self.present(PSPDFNavigationController(rootViewController: pdfController), animated: true, completion: nil)
        }
//        document?.open(completionHandler: { (success) in
//            if success {
//                let configuration = PSPDFConfiguration(builder: { builder in
//                    builder.thumbnailBarMode = .scrubberBar
//                    builder.isPageLabelEnabled = false
//                    builder.isTextSelectionEnabled = false
//                    builder.isAutosaveEnabled = false
//                    builder.applicationActivities = [
//                        UIActivityType.mail,
//                        UIActivityType.message,
//                    ]
//                    builder.excludedActivityTypes = [
//                        PSPDFActivityTypeOpenIn,
//                        UIActivityType.print.rawValue,
//                        UIActivityType.copyToPasteboard.rawValue,
//                        UIActivityType.assignToContact.rawValue,
//                        UIActivityType.postToFacebook.rawValue,
//                        UIActivityType.postToTwitter.rawValue
//                    ]
//                })
//                let doc = PSPDFDocument(url: self.document!.fileURL)
//                let pdfController = PSPDFViewController(document: doc, configuration: configuration)
//                var r = pdfController.navigationItem.rightBarButtonItems(for: .document)
//                if r != nil {
//                    if let index = r?.index(of: pdfController.activityButtonItem) {
//                        r?.remove(at: index)
//                    }
//                    pdfController.navigationItem.setRightBarButtonItems(r!, for: .document, animated: true)
//                }
//                self.present(PSPDFNavigationController(rootViewController: pdfController), animated: true, completion: nil)
//            } else {
                // Make sure to handle the failed import appropriately, e.g., by presenting an error message to the user.
//            }
//        })
    }
    
    @IBAction func dismissDocumentViewController() {
        dismiss(animated: true) {
            self.fileUrl?.stopAccessingSecurityScopedResource()
        }
    }
}
